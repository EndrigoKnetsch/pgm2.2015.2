package aula;


public class IntegerSet {
    private final int SIZE = 101;
    private boolean[] valores;


    public IntegerSet() {
        this.valores = new boolean[SIZE];
    }

    public boolean adicionarElemento(int valor) {
        if (valor >= 0 && valor < SIZE) {
            this.valores[valor] = true;
            return true;
        }
        return false;
    }

    public boolean contem(int valor) {
        return valor >= 0 && valor < SIZE && this.valores[valor];
    }

    public boolean equals(IntegerSet other) {
        if (other == null)
            return false;
        for (int i = 0; i < SIZE; i++) {
            if (this.valores[i] != other.valores[i]) {
                return false;
            }
        }
        return true;
    }

    public IntegerSet uniao(IntegerSet other) {
        IntegerSet result = new IntegerSet();
        if (other == null) {
            for(int i = 0; i < SIZE; i++) {
                if(this.contem(i)) {
                    result.adicionarElemento(i);
                }
            }
            return result;
        }
        for(int i = 0; i < SIZE; i++) {
            if (this.contem(i) || other.contem(i)) {
                result.adicionarElemento(i);
            }
        }
        return result;
    }

    public IntegerSet intersecao(IntegerSet other) {
        IntegerSet result = new IntegerSet();
        if (other == null) {
            return result;
        }
        for(int i = 0; i < SIZE; i++) {
            if (this.contem(i) && other.contem(i)) {
                result.adicionarElemento(i);
            }
        }
        return result;
    }

    public String toString() {
        String result = "{";
        for (int i = 0; i < SIZE; i++) {
            if (this.valores[i]) {
                if (result.length() == 1) {
                    result = result + i;
                } else {
                    result = result + ", " + i;
                }
            }
        }
        return result + "}";
    }

}
